﻿using JeuSociete.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;


namespace JeuSociete
{
    public class PlateauJeuQuadrille<T>
    {
        private Case<T>[,] _cases;
        private readonly int _hauteur;
        private readonly int _largeur;

   

        public PlateauJeuQuadrille(int hauteur, int largeur)
        {
            _hauteur = hauteur;
            _largeur = largeur;
            InitialiserPlateau();
        }


        private void InitialiserPlateau()
        {
            _cases = new Case<T>[_hauteur, _largeur];
            for (int i = 0; i < _hauteur; i++)
            {
                for (int j = 0; j < _largeur; j++)
                {
                    _cases[i, j] = new Case<T>();
                }
            }
        }

        public Case<T> GetCase(CoordonneesPlateau emplacement)
        {
            Contract.Requires<ArgumentNullException>(emplacement != null);
            Contract.Requires<CoordonneesHorsPlateauException>(SontCoordonneesValides(emplacement));
            return _cases[emplacement.X, emplacement.Y];
        }

        public T GetContenuCase(CoordonneesPlateau emplacement)
        {
            Contract.Requires<ArgumentNullException>(emplacement != null);
            Contract.Requires<CoordonneesHorsPlateauException>(SontCoordonneesValides(emplacement));
            return GetCase(emplacement).ContenuCase;
        }

        public void SetContenuCase(CoordonneesPlateau emplacement, T contenu)
        {
           
            Contract.Requires<ArgumentNullException>(emplacement != null);
            _cases[emplacement.X, emplacement.Y].ContenuCase = contenu;
        }

        //pourquoi avoir utiliser default https://msdn.microsoft.com/en-us/library/xwth0h0d.aspx
        public void DeplacerContenu(SegmentPlateau deplacement, T contenuAMettreOrigine = default(T)) 
        {
            Contract.Requires<ArgumentNullException>(deplacement != null);
            GetCase(deplacement.Destination).ContenuCase = GetCase(deplacement.Origine).ContenuCase;
            GetCase(deplacement.Origine).ContenuCase = contenuAMettreOrigine;
        }

        public IList<CoordonneesPlateau> ObtenirCasesSurChemin(SegmentPlateau segment)
        {
            Contract.Requires<ArgumentNullException>(segment != null);
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            if (segment.EstSegmentDiagonal45Degres())
            {
                cases = ObtenirCasesEntre2CasesDiagonales(segment);
            }else if (segment.EstSegmentDroit())
            {
                cases = ObtenirCasesEntre2CasesLigneDroite(segment);
            }
            return cases;
        }

      
        private List<CoordonneesPlateau> ObtenirCasesEntre2CasesLigneDroite(SegmentPlateau segment)
        {
            Contract.Requires<ArgumentException>((segment.Origine.X == segment.Destination.X) || segment.Origine.Y == segment.Destination.Y); //valider qu'on a bien un segment droit
            Contract.Requires<ArgumentNullException>(segment != null);
            int offSetX = Math.Abs(segment.Origine.X - segment.Destination.X);
            var cases = offSetX == 0 ? ObtenirCasesVerticalesEntrePoints(segment) : ObtenirCasesHorizontalesEntrePoints(segment);
            return cases;
        }

        private List<CoordonneesPlateau> ObtenirCasesEntre2CasesDiagonales(SegmentPlateau segment)
        {
            Contract.Requires<ArgumentNullException>(segment != null);
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            CoordonneesPlateau pointOuest = (segment.Origine.X < segment.Destination.X) ? segment.Origine : segment.Destination;
            CoordonneesPlateau pointEst = (segment.Origine.X < segment.Destination.X) ? segment.Destination : segment.Origine;
            if (pointOuest.Y < pointEst.Y)
            {
                int nbCases = Math.Abs(pointOuest.X - pointEst.X)-1;
                for (int i = 1; i <= nbCases; i++)
                {
                    cases.Add(new CoordonneesPlateau(pointOuest.X + i, pointOuest.Y + i));
                }
            }
            else
            {
                int nbCases = Math.Abs(pointOuest.X - pointEst.X)-1;
                for (int i = 1; i <= nbCases; i++)
                {
                    cases.Add(new CoordonneesPlateau(pointOuest.X + i, pointOuest.Y - i));
                }
            }
            return cases;
        }

        private List<CoordonneesPlateau> ObtenirCasesHorizontalesEntrePoints(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            CoordonneesPlateau pointOuest = (segment.Origine.X < segment.Destination.X) ? segment.Origine : segment.Destination;
            CoordonneesPlateau pointEst = (segment.Origine.X < segment.Destination.X) ? segment.Destination : segment.Origine;
            int nbCases = pointEst.X - pointOuest.X;
            for (int i = 1; i < nbCases; i++)
            {
                cases.Add(new CoordonneesPlateau(pointOuest.X + i, pointOuest.Y));
            }
            return cases;
        }

        private List<CoordonneesPlateau> ObtenirCasesVerticalesEntrePoints(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            CoordonneesPlateau pointSud = (segment.Origine.Y < segment.Destination.Y) ? segment.Origine : segment.Destination;
            CoordonneesPlateau pointNord = (segment.Origine.Y < segment.Destination.Y) ? segment.Destination : segment.Origine;
            int nbCases = pointNord.Y - pointSud.Y;
            for (int i = 1; i < nbCases; i++)
            {
                cases.Add(new CoordonneesPlateau(pointSud.X, pointSud.Y + i));
            }
            return cases;
        }

        public bool SontCoordonneesValides(CoordonneesPlateau emplacement)
        {
            return emplacement.X < _largeur-1 && emplacement.X > 0 && emplacement.Y < _hauteur-1 && emplacement.Y > 0;
        }
    }
}
