﻿using System.Drawing;
using System.Windows.Forms;
using Echec;

namespace EchecUI
{

    class CelluleEchiquier : PictureBox
    {
        public static readonly System.Drawing.Size _tailleCellules = new System.Drawing.Size(50, 50);
        public readonly int rangee, colonne;
        public Piece ContenuCellule { get; set; }
              
        public CelluleEchiquier(int rangee, int colonne)// : base() 
        {
            this.rangee = rangee; 
            this.colonne = colonne;
            this.Size = _tailleCellules;
            this.ContenuCellule = null;
            this.BackColor = ObtenirCouleurCase(colonne, rangee);
        }

        public override string ToString() { return "Cellule(" + rangee + "," + colonne + ")"; }

        public void ResetCouleurCase()
        {
            this.BackColor = ObtenirCouleurCase(colonne, rangee);
        }

        public static Color ObtenirCouleurCase(int colonne, int rangee)
        {
            return (colonne % 2 == rangee % 2) ? Color.LightGreen : Color.DarkGreen;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            using (Font myFont = new Font("Arial", 14))
            {
                if (ContenuCellule != null)
                {
                    Brush couleur = ContenuCellule.Couleur == Couleur.Blanc ? Brushes.White : Brushes.Black;
                    pe.Graphics.DrawString(ContenuCellule.Type.ToString().Substring(0,3), myFont, couleur, new Point(2, 2));

                }
            }
        }

    }
}
