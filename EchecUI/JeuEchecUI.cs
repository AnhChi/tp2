﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Echec;
using JeuSociete;


namespace EchecUI
{
   public partial class JeuEchecUI : Form {
        private readonly TableLayoutPanel _cellules;
        private readonly JeuEchec _jeuEchec = new JeuEchec();
        private CoordonneesPlateau _caseChoisie;
        private CoordonneesPlateau _caseDestination;

        public JeuEchecUI() {
            InitializeComponent();
            _cellules = ObtenirEchiquier();
            this.Controls.Add(_cellules);
        }

        private TableLayoutPanel ObtenirEchiquier() {
            TableLayoutPanel panneau = new TableLayoutPanel
            {
                ColumnCount = 8,
                RowCount = 8
            };
            for (int i = 0; i < panneau.ColumnCount; i++) { 
                panneau.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, CelluleEchiquier._tailleCellules.Width)); 
            }
            for (int i = 0; i < panneau.RowCount; i++) {
                panneau.RowStyles.Add(new RowStyle(SizeType.Absolute, CelluleEchiquier._tailleCellules.Height)); 
            }
            for (int rangee = 0;rangee < panneau.RowCount ; rangee++) {
                for (int col = 0; col < panneau.ColumnCount; col++) {
                    CelluleEchiquier cellule = new CelluleEchiquier(rangee, col);
                    CoordonneesPlateau coordonneesCase = new CoordonneesPlateau(col,7-rangee);
                    if(_jeuEchec.GetContenuCase(coordonneesCase) != null){
                        cellule.ContenuCellule = _jeuEchec.GetContenuCase(coordonneesCase);
                    }
                    cellule.Click += new EventHandler(cellule_Click);
                    panneau.Controls.Add(cellule, col, rangee);
                }
            }
            panneau.Padding = new Padding(0);
            panneau.Size = new System.Drawing.Size(panneau.ColumnCount * CelluleEchiquier._tailleCellules.Width, panneau.RowCount * CelluleEchiquier._tailleCellules.Height);
            return panneau;
        }

        private void cellule_Click(object sender, EventArgs e) {
            CelluleEchiquier cellule = (CelluleEchiquier)sender;
            TraitementClick(cellule);
        }

        private void TraitementClick(CelluleEchiquier celluleCliquee)
        {
            int colonne = celluleCliquee.colonne;
            int rangee = celluleCliquee.rangee;
            if (celluleCliquee.ContenuCellule != null || _caseChoisie != null)
            {
                if (_caseChoisie == null)
                {
                    celluleCliquee.BackColor = Color.Red;
                    _caseChoisie = new CoordonneesPlateau(colonne, rangee);
                }
                else if (_caseChoisie.X == colonne && _caseChoisie.Y == rangee)
                {
                    DecocherCaseSelectionnee(celluleCliquee, colonne, rangee);
                }
                else
                {
                    _caseDestination = new CoordonneesPlateau(colonne, rangee);
                    CoordonneesPlateau caseDestination = new CoordonneesPlateau(colonne, 7-rangee);
                    CoordonneesPlateau caseOrigine = new CoordonneesPlateau(_caseChoisie.X, 7-_caseChoisie.Y);
                    if (_jeuEchec.TryDeplacerPiece(caseOrigine, caseDestination))
                    {
                        EffectuerDeplacement(celluleCliquee);
                    }
                }
            }
        }

        private void DecocherCaseSelectionnee(CelluleEchiquier cellule, int colonne, int rangee)
        {
            cellule.BackColor = CelluleEchiquier.ObtenirCouleurCase(colonne, rangee);
            _caseChoisie = null;
        }

        private void EffectuerDeplacement(CelluleEchiquier cellule)
        {
            CelluleEchiquier celluleOrigine = (CelluleEchiquier)_cellules.GetControlFromPosition(_caseChoisie.X, _caseChoisie.Y);
            celluleOrigine.ResetCouleurCase();
            cellule.ResetCouleurCase();

            CoordonneesPlateau coordonneesCaseChoisie = new CoordonneesPlateau(_caseChoisie.X, 7 - _caseChoisie.Y);
            celluleOrigine.ContenuCellule = _jeuEchec.GetContenuCase(coordonneesCaseChoisie);
            CoordonneesPlateau coordonneesCaseDestination = new CoordonneesPlateau(_caseDestination.X, 7 - _caseDestination.Y);
            cellule.ContenuCellule = _jeuEchec.GetContenuCase(coordonneesCaseDestination);
            celluleOrigine.Refresh();
            cellule.Refresh();
            _caseChoisie = null;
            _caseDestination = null;
        }
    }
}
