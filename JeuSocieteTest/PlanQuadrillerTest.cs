﻿using System;
using System.Collections.Generic;
using JeuSociete;
using JeuSociete.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JeuSocieteTest
{
    [TestClass]
    public class PlanQuadrillerTest
    {
        [TestMethod]
        public void ObtenirCasesSurCheminTest()
        {
            CoordonneesPlateau point1 = new CoordonneesPlateau(1, 1);
            CoordonneesPlateau point2 = new CoordonneesPlateau(5, 1);
            SegmentPlateau seg = new SegmentPlateau(point1, point2);
            PlateauJeuQuadrille<string> planquadrille = new PlateauJeuQuadrille<string>(8, 8);
            IList<CoordonneesPlateau> coordonneesCases = planquadrille.ObtenirCasesSurChemin(seg);
            Assert.AreEqual(3, coordonneesCases.Count);
        }

        [TestMethod]
        public void ObtenirCasesSurCheminDiagonalTest()
        {
            CoordonneesPlateau point1 = new CoordonneesPlateau(3, 2);
            CoordonneesPlateau point2 = new CoordonneesPlateau(5, 0);
            SegmentPlateau seg = new SegmentPlateau(point1, point2);
            PlateauJeuQuadrille<string> planquadrille = new PlateauJeuQuadrille<string>(8, 8);
            IList<CoordonneesPlateau> coordonneesCases = planquadrille.ObtenirCasesSurChemin(seg);
            Assert.AreEqual(1, coordonneesCases.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetContenuCaseCoordonneesNull()
        {
            PlateauJeuQuadrille<string> planquadrille = new PlateauJeuQuadrille<string>(8, 8);
            planquadrille.GetContenuCase(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetCaseCoordonneesNull()
        {
            PlateauJeuQuadrille<string> planquadrille = new PlateauJeuQuadrille<string>(8, 8);
            planquadrille.GetCase(null);
        }

        [TestMethod]
        [ExpectedException(typeof(CoordonneesHorsPlateauException))]
        public void GetContenuCaseCoordonneesNonValides()
        {
            PlateauJeuQuadrille<string> planquadrille = new PlateauJeuQuadrille<string>(8, 8);
            CoordonneesPlateau coordonneesInvalides = new CoordonneesPlateau(8,8);
            planquadrille.GetContenuCase(coordonneesInvalides);
        }
    }
}
