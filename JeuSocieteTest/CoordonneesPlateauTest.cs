﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JeuSociete;

namespace JeuSocieteTest
{
    [TestClass]
    public class CoordonneesPlateauTest
    {
        [TestMethod]
        public void EstMemeEndroitEndroitNullTest()
        {
            CoordonneesPlateau desCoordonnees = new CoordonneesPlateau(0, 0);

            bool estMemeEndroit = desCoordonnees.EstMemeEndroit(null);

            Assert.AreEqual(false, estMemeEndroit);
        }

        [TestMethod]
        public void EstMemeEndroitMemeEndroitTest()
        {
            CoordonneesPlateau desCoordonnees = new CoordonneesPlateau(0, 0);
            CoordonneesPlateau autresCoordonnees = new CoordonneesPlateau(0, 0);

            bool estMemeEndroit = desCoordonnees.EstMemeEndroit(autresCoordonnees);

            Assert.AreEqual(true, estMemeEndroit);
        }

        [TestMethod]
        public void EstMemeEndroitPasMemeTest()
        {
            CoordonneesPlateau desCoordonnees = new CoordonneesPlateau(0, 0);
            CoordonneesPlateau autresCoordonnees = new CoordonneesPlateau(1, 0);

            bool estMemeEndroit = desCoordonnees.EstMemeEndroit(autresCoordonnees);

            Assert.AreEqual(false, estMemeEndroit);
        }
    }
}
