﻿namespace Echec.Regles
{
    public class CaseOccupable : IRegle
    {
        private readonly Deplacement _deplacementVoulu;
        public CaseOccupable(Deplacement deplacement)
        {
            _deplacementVoulu = deplacement;
        }

        public bool EstRespectee()
        {
            Piece caseDestination = _deplacementVoulu.ContenuCaseDestination;
            if (caseDestination != null && caseDestination.Couleur == _deplacementVoulu.PieceCaseOrigine.Couleur)
            {
                return false;
            }
            return true;
        }

        public string ObtenirMessageErreur()
        {
            return "La pièce " + _deplacementVoulu.ObtenirTypePieceChoisie() + " ne peut pas capturer son allié";
        }
    }
}
