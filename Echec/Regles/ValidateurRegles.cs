﻿using System.Collections.Generic;
using JeuSociete;
using Echec.Regles;


namespace Echec
{
    public class ValidateurRegles
    {
        private IList<IRegle> _regles;
        public IList<string> Messages { get; set; }

        public ValidateurRegles(Deplacement deplacement, PlateauJeuQuadrille<Piece> plateau)
        {
            _regles = new List<IRegle>();
            _regles.Add(new MouvementNonObstrue(deplacement, plateau));
            _regles.Add(new PieceMouvementPossible(deplacement));
            _regles.Add(new PionCapture(deplacement));
            _regles.Add(new CaseOccupable(deplacement));
            Messages = new List<string>();

           
        }

        public void ValiderRegles()
        {
            foreach (var regle in _regles)
            {
                if (!regle.EstRespectee())
                {
                    Messages.Add(regle.ObtenirMessageErreur());
                }
            }
        }


    }
}
