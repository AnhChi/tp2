﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Echec
{
    public interface IRegle
    {
        bool EstRespectee();
        string ObtenirMessageErreur();
    }
}
