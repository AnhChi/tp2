﻿using System;
using System.Collections.Generic;
using JeuSociete;

namespace Echec
{
    public class Echiquier
    {
        readonly PlateauJeuQuadrille<Piece> _plateau;
        readonly FabriquePiece _fabrique;

        public Echiquier()
        {
            _plateau = new PlateauJeuQuadrille<Piece>(8, 8);
            _fabrique = new FabriquePiece();
            CreerPieces(1, 0, Couleur.Blanc);
            CreerPieces(6, 7, Couleur.Noir);
        }

        public Piece ObtenirPiece(CoordonneesPlateau emplacementPiece){
            Case<Piece> caseChoisie = _plateau.GetCase(emplacementPiece);
            Piece pieceChoisie = caseChoisie.ContenuCase;
            return pieceChoisie;
        }

        public void Deplacer(SegmentPlateau deplacement)
        {
            Piece pieceChoisie = ObtenirPiece(deplacement.Origine);
            pieceChoisie.ADejaBouger = true;
            _plateau.DeplacerContenu(deplacement);
        }
        
        public IList<string> ObtenirMessagesSurDeplacementVoulu(Deplacement deplacement)
        {
            string messageSimulation = string.Empty;
            IList<string> messages = ObtenirMessagesReglesNonRespectees(deplacement);
            if (messages.Count == 0)
            {
                 messageSimulation = ObtenirMessageSimulationAttaqueRoi(deplacement);
                 if (messageSimulation != string.Empty)
                 {
                     messages.Add(messageSimulation);
                 }
            }
            return messages;
        }

        private IList<string> ObtenirMessagesReglesNonRespectees(Deplacement deplacement)
        {
            IList<string> messages = new List<string>();
            ValidateurRegles validateur = new ValidateurRegles(deplacement, _plateau);
            validateur.ValiderRegles();
            if (validateur.Messages != null)
            {
                messages = validateur.Messages;
            }
            return messages;
        }

        private string ObtenirMessageSimulationAttaqueRoi(Deplacement deplacement)
        {
            string message = "";
            _plateau.DeplacerContenu(deplacement.Segment);
            Couleur couleurPieceChoisie = deplacement.ObtenirCouleurPieceChoisie();
            if (RoiEstEnEchec(couleurPieceChoisie))
            {
                message =  "Ce mouvement met votre propre roi en echec ou le laisse en échec";
            }
            //rollback
            _plateau.SetContenuCase(deplacement.Segment.Origine, deplacement.PieceCaseOrigine);
            _plateau.SetContenuCase(deplacement.Segment.Destination, deplacement.ContenuCaseDestination);
            return message;
        }

        public bool RoiEstEnEchec(Couleur couleurJoueurCourrant)
        {
            CoordonneesPlateau emplacementRoiCouleurJouee = ObtenirCoordonneesRoi(couleurJoueurCourrant);
            Piece roi = ObtenirPiece(emplacementRoiCouleurJouee);
            Piece attaquantPossible;
            SegmentPlateau segmentDeplacement;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    CoordonneesPlateau origineAttaquantPossible = new CoordonneesPlateau(i, j);
                    attaquantPossible = _plateau.GetCase(origineAttaquantPossible).ContenuCase;
                    if (attaquantPossible != null && attaquantPossible.Couleur != couleurJoueurCourrant)
                    {
                        segmentDeplacement = new SegmentPlateau(origineAttaquantPossible, emplacementRoiCouleurJouee);
                        Deplacement deplacementFictifAttaque = new Deplacement(segmentDeplacement, attaquantPossible, roi);
                        IList<string> messages = ObtenirMessagesReglesNonRespectees(deplacementFictifAttaque);
                        if (messages.Count == 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool RoiEstEchecEtMat(Couleur couleurJoueurCourrant)
        {
            bool roiPeutEtreDefendu;
            CoordonneesPlateau roiCoordonnees = ObtenirCoordonneesRoi(couleurJoueurCourrant);
            var roi = _plateau.GetContenuCase(roiCoordonnees);
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    CoordonneesPlateau origineDefenseurPossible = new CoordonneesPlateau(i, j);
                    var defenseurPossible = _plateau.GetContenuCase(origineDefenseurPossible);
                    if (defenseurPossible == null || defenseurPossible.Couleur != couleurJoueurCourrant) continue;
                        roiPeutEtreDefendu = PeutDefendre(defenseurPossible, origineDefenseurPossible);
                        var segmentDeplacement = new SegmentPlateau(origineDefenseurPossible, origineDefenseurPossible);
                        var deplacementFictifAttaque = new Deplacement(segmentDeplacement, defenseurPossible, roi);
                        var messages = ObtenirMessagesReglesNonRespectees(deplacementFictifAttaque);
                        if (messages.Count == 0)
                        {
                            return true;
                        }
                }
            }
            return false;
        }

        private bool PeutDefendre(Piece defenseurPossible, CoordonneesPlateau origineDefenseurPossible)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var destination = new CoordonneesPlateau(i, j);
                    Piece contenuDestination = ObtenirPiece(destination);
                    SegmentPlateau segment = new SegmentPlateau(origineDefenseurPossible, destination);
                    Deplacement deplacement = new Deplacement(segment, defenseurPossible, contenuDestination);
                    ValidateurRegles validateur = new ValidateurRegles(deplacement, _plateau);
                    validateur.ValiderRegles();
                    if (validateur.Messages == null || validateur.Messages.Count == 0)
                    {
                        _plateau.DeplacerContenu(new SegmentPlateau(origineDefenseurPossible, destination));
                        if (!RoiEstEnEchec(defenseurPossible.Couleur))
                        {
                            return true;
                        }
                        //rollback
                        _plateau.SetContenuCase(origineDefenseurPossible, defenseurPossible);
                        _plateau.SetContenuCase(destination, contenuDestination);
                    }
                }
            }
            return false;
        }

        private void CreerPieces(int indexPion, int indexAutresPieces, Couleur couleur)
        {
            AjouterAutresPieces(indexPion, indexAutresPieces, couleur);
            AjouterPions(indexPion, couleur);
        }

        private void AjouterAutresPieces(int indexPion, int indexAutresPieces, Couleur couleur)
        {
            AjouterPiece(0, indexAutresPieces, couleur, TypesPiece.Tour);
            AjouterPiece(1, indexAutresPieces, couleur, TypesPiece.Cavalier);
            AjouterPiece(2, indexAutresPieces, couleur, TypesPiece.Fou);
            AjouterPiece(3, indexAutresPieces, couleur, TypesPiece.Reine);
            AjouterPiece(4, indexAutresPieces, couleur, TypesPiece.Roi);
            AjouterPiece(5, indexAutresPieces, couleur, TypesPiece.Fou);
            AjouterPiece(6, indexAutresPieces, couleur, TypesPiece.Cavalier);
            AjouterPiece(7, indexAutresPieces, couleur, TypesPiece.Tour);
        }

        private void AjouterPions(int indexPion, Couleur couleur)
        {
            AjouterPiece(0, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(1, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(2, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(3, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(4, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(5, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(6, indexPion, couleur, TypesPiece.Pion);
            AjouterPiece(7, indexPion, couleur, TypesPiece.Pion);
        }

        private void AjouterPiece(int indexPion, int indexAutresPieces, Couleur couleur, TypesPiece type)
        {
            CoordonneesPlateau coordonneesPieceAAjouter = new CoordonneesPlateau(indexPion, indexAutresPieces); ;
            Piece pieceAAjouter = _fabrique.FabriquerPiece(type, couleur);
            _plateau.SetContenuCase(coordonneesPieceAAjouter, pieceAAjouter);
        }
    
        private CoordonneesPlateau ObtenirCoordonneesRoi(Couleur couleur)
        {
            Piece unePiece;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    unePiece = _plateau.GetCase(new CoordonneesPlateau(i, j)).ContenuCase;
                    if (unePiece != null && unePiece.Couleur == couleur && unePiece.Type == TypesPiece.Roi)
                    {
                        return new CoordonneesPlateau(i, j);
                    }
                }
            }
            throw new Exception("Roi " + couleur + " Non Trouvé");
        }
    }

}
