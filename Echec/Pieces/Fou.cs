﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec
{
    public class Fou:Piece
    {
        

        public override TypesPiece Type
        {
            get
            {
                return TypesPiece.Fou;
            }
        }

     

        public override bool DeplacementPossible(SegmentPlateau deplacementVoulu)
        {
            return deplacementVoulu.EstSegmentDiagonal45Degres();
        }
       
    }
}
