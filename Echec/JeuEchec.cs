﻿using System.Collections.Generic;
using JeuSociete;

namespace Echec
{
    public class JeuEchec
    {
        private readonly Echiquier _echiquier;
        Couleur _couleurJoueurDoitJouer = Couleur.Blanc; //Les blancs commencent

        public IList<string> Messages { get; set; }
        public bool RoiEchecEtMat { get; set; }
        public bool RoiEchec { get; set; }

        public JeuEchec()
        {
            _echiquier = new Echiquier();
        }

        public void ViderListeMessage()
        {
            Messages.Clear();
        }

        public Piece GetContenuCase(CoordonneesPlateau coordonneesCase)
        {
            return _echiquier.ObtenirPiece(coordonneesCase);
        }

       
        public bool TryDeplacerPiece(CoordonneesPlateau origine, CoordonneesPlateau destination)
        {
            Piece pieceChoisie = _echiquier.ObtenirPiece(origine);
            Piece caseDestination = _echiquier.ObtenirPiece(destination);
            if (pieceChoisie == null || pieceChoisie.Couleur != _couleurJoueurDoitJouer)
            {
                return false; //C'est pas son tour
            }
            SegmentPlateau segment = new SegmentPlateau(origine, destination);
            Deplacement deplacementVoulu = new Deplacement(segment, pieceChoisie, caseDestination);
            Messages = _echiquier.ObtenirMessagesSurDeplacementVoulu(deplacementVoulu);
            if (Messages.Count == 0)
            {
                _echiquier.Deplacer(segment);
                _couleurJoueurDoitJouer = (_couleurJoueurDoitJouer == Couleur.Blanc) ? Couleur.Noir : Couleur.Blanc;
                RoiEchec = _echiquier.RoiEstEnEchec(_couleurJoueurDoitJouer);
                if (RoiEchec)
                {
                    RoiEchecEtMat = _echiquier.RoiEstEchecEtMat(_couleurJoueurDoitJouer);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
