﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Echec;
using JeuSociete;
namespace EchecTest
{
    [TestClass]
    public class ReineTest
    {
        [TestMethod]
        public void MouvementPossibleTest()
        {
            int x = 2;
            int y = 4;
            Piece unePiece = new Reine();
            unePiece.Couleur = Couleur.Blanc;
            CoordonneesPlateau origine = new CoordonneesPlateau(x, y);
            SegmentPlateau mouvement1 = new SegmentPlateau(origine, new CoordonneesPlateau(1, 3));
            SegmentPlateau mouvement2 = new SegmentPlateau(origine, new CoordonneesPlateau(1, 4));
            SegmentPlateau mouvement3 = new SegmentPlateau(origine, new CoordonneesPlateau(3, 3));
            SegmentPlateau mouvement4 = new SegmentPlateau(origine, new CoordonneesPlateau(4, 6));
            Assert.AreEqual(true, unePiece.DeplacementPossible(mouvement1));
            Assert.AreEqual(true, unePiece.DeplacementPossible(mouvement2));
            Assert.AreEqual(true, unePiece.DeplacementPossible(mouvement3));
            Assert.AreEqual(true, unePiece.DeplacementPossible(mouvement4));
        }

       
    }
}
